The NERDTree/libre
==================

Version
-------

0.03

Introduction
------------

The NERDTree/libre is liberated fork of The NERDTree.

The NERDTree is a file system explorer for the Vim editor. Using this plugin,
users can visually browse complex directory hierarchies, quickly open files for
reading or editing, and perform basic file system operations.

This plugin can also be extended with custom mappings using a special API. The
details of this API and of other NERDTree features are described in the
included documentation.

## Screenshots

![Alt Text](screenshot.png "The NERDTree/libre screenshot")

Installation
------------

#### [pathogen.vim](https://github.com/tpope/vim-pathogen)

    git clone https://gitlab.com/neva_blyad/nerdtree-libre.git ~/.vim/bundle/nerdtree

Then reload Vim, run `:helptags ~/.vim/bundle/nerdtree/doc/` or `:Helptags`, and check out `:help NERDTree.txt`.


#### [apt-vim](https://github.com/egalpin/apt-vim)

    apt-vim install -y https://gitlab.com/neva_blyad/nerdtree-libre.git

F.A.Q.
------

> Is there any support for `git` flags?

Yes, install [nerdtree-git-plugin](https://github.com/Xuyuanp/nerdtree-git-plugin).

---

> Can I have the nerdtree on every tab automatically?

Nope. If this is something you want then chances are you aren't using tabs and
buffers as they were intended to be used. Read this
http://stackoverflow.com/questions/102384/using-vims-tabs-like-buffers

If you are interested in this behaviour then consider [vim-nerdtree-tabs](https://github.com/jistr/vim-nerdtree-tabs)

---
> How can I open a NERDTree automatically when vim starts up?

Stick this in your vimrc: `autocmd vimenter * NERDTree`

---
> How can I open a NERDTree automatically when vim starts up if no files were specified?

Stick this in your vimrc:

    autocmd StdinReadPre * let s:std_in=1
    autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

Note: Now start vim with plain `vim`, not `vim .`

---
> How can I open NERDTree automatically when vim starts up on opening a directory?

    autocmd StdinReadPre * let s:std_in=1
    autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

This window is tab-specific, meaning it's used by all windows in the tab. This trick also prevents NERDTree from hiding when first selecting a file.

Note: Executing `vim ~/some-directory` will open NERDTree and a new edit window. `exe 'cd '.argv()[0]` sets the `pwd` of the new edit window to `~/some-directory`

---
> How can I map a specific key or shortcut to open NERDTree?

Stick this in your vimrc to open NERDTree with `Ctrl+n` (you can set whatever key you want):

    map <C-n> :NERDTreeToggle<CR>

---
> How can I close vim if the only window left open is a NERDTree?

Stick this in your vimrc:

    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

---
> Can I have different highlighting for different file extensions?

See here: https://github.com/scrooloose/nerdtree/issues/433#issuecomment-92590696

---
> How can I change default arrows?

Use these variables in your vimrc. Note that below are default arrow symbols

    let g:NERDTreeDirArrowExpandable = '▸'
    let g:NERDTreeDirArrowCollapsible = '▾'

## Copyright and License

    Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                            <neva_blyad@lovecri.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Authors

    Sam Hocevar <sam@hocevar.net>
    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
