let s:tree_up_dir_line = '.. (up a dir)'
syn match NERDTreeIgnore #\~#
exec 'syn match NERDTreeIgnore #\['.g:NERDTreeGlyphReadOnly.'\]#'

"highlighting for the .. (up dir) line at the top of the tree
execute "syn match NERDTreeUp #\\V". s:tree_up_dir_line ."#"

"quickhelp syntax elements
syn match NERDTreeHelpKey #" \{1,2\}[^ ]*:#ms=s+2,me=e-1
syn match NERDTreeHelpKey #" \{1,2\}[^ ]*,#ms=s+2,me=e-1
syn match NERDTreeHelpTitle #" .*\~$#ms=s+2,me=e-1
syn match NERDTreeToggleOn #(on)#ms=s+1,he=e-1
syn match NERDTreeToggleOff #(off)#ms=e-3,me=e-1
syn match NERDTreeHelpCommand #" :.\{-}\>#hs=s+3
syn match NERDTreeHelp  #^".*# contains=NERDTreeHelpKey,NERDTreeHelpTitle,NERDTreeIgnore,NERDTreeToggleOff,NERDTreeToggleOn,NERDTreeHelpCommand

"highlighing for directory nodes and file nodes
syn match NERDTreeDirSlash #/# containedin=NERDTreeDir

exec 'syn match NERDTreeClosable #' . escape(g:NERDTreeDirArrowCollapsible, '~') . '\ze .*/# containedin=NERDTreeDir,NERDTreeFile'
exec 'syn match NERDTreeOpenable #' . escape(g:NERDTreeDirArrowExpandable, '~') . '\ze .*/# containedin=NERDTreeDir,NERDTreeFile'

let s:dirArrows = escape(g:NERDTreeDirArrowCollapsible, '~]\-').escape(g:NERDTreeDirArrowExpandable, '~]\-')

let s:exec_file       = '\*'
let s:libra_file      = '\.\(epub\|pdf\|fb2\|azw\|azw3\|kfx\|mobi\|lit\|lrf\|djvu\|html\|htm\|css\|doc\|docx\|rtf\|txt\|odf\)'
let s:codibra_file    = '\.\(c\|h\|pl\|pm\|cpp\|cc\|hh\|vim\|py\|vala\|vapi\)'
let s:audio_file      = '\.\(aac\|au\|flac\|m4a\|mid\|midi\|mka\|mp3\|mpc\|ogg\|ra\|wav\|oga\|opus\|spx\|xspf\)'
let s:imagevideo_file = '\.\(jpg\|jpeg\|mjpg\|mjpeg\|gif\|bmp\|pbm\|pgm\|ppm\|tga\|xbm\|xpm\|tif\|tiff\|png\|svg\|svgz\|mng\|pcx\|mov\|mpg\|mpeg\|m2v\|mkv\|webm\|ogm\|mp4\|m4v\|mp4v\|vob\|qt\|nuv\|wmv\|asf\|rm\|rmvb\|flc\|avi\|fli\|flv\|gl\|dl\|xcf\|xwd\|yuv\|cgm\|emf\|ogv\|ogx\|nef\)'
let s:archive_file    = '\.\(tar\|tgz\|arc\|arj\|taz\|lha\|lz4\|lzh\|lzma\|tlz\|txz\|tzo\|t7z\|zip\|z\|dz\|gz\|lrz\|lz\|lzo\|xz\|zst\|tzst\|bz2\|bz\|tbz\|tbz2\|tz\|deb\|rpm\|jar\|war\|ear\|sar\|rar\|alz\|ace\|zoo\|cpio\|7z\|rz\|cab\|wim\|swm\|dwmesd\|img\|iso\|fs\)'

exec 'syn match NERDTreeDir                  #[^'.s:dirArrows.' ].*/#'
exec 'syn match NERDTreeLink                 # \zs.*\ze -># contains=NERDTreeBookmark'

exec 'syn match NERDTreeExecFile             #^[^>]*'.s:exec_file.'$# contains=NERDTreeBookmark'
exec 'syn match NERDTreeLibraFile            #^[^>]*'.s:libra_file.'$# contains=NERDTreeBookmark'
exec 'syn match NERDTreeCodibraFile          #^[^>]*'.s:codibra_file.'$# contains=NERDTreeBookmark'
exec 'syn match NERDTreeAudioFile            #^[^>]*'.s:audio_file.'$# contains=NERDTreeBookmark'
exec 'syn match NERDTreeImageVideoFile       #^[^>]*'.s:imagevideo_file.'$# contains=NERDTreeBookmark'
exec 'syn match NERDTreeArchiveFile          #^[^>]*'.s:archive_file.'$# contains=NERDTreeBookmark'

exec 'syn match NERDTreeExecFileTarget       #> \zs.*'.s:exec_file.'\($\|\ze \[[^\]]*\]$\)# contains=NERDTreeBookmark'
exec 'syn match NERDTreeLibraFileTarget      #> \zs.*'.s:libra_file.'\($\|\ze \[[^\]]*\]$\)# contains=NERDTreeBookmark'
exec 'syn match NERDTreeCodibraFileTarget    #> \zs.*'.s:codibra_file.'\($\|\ze \[[^\]]*\]$\)# contains=NERDTreeBookmark'
exec 'syn match NERDTreeAudioFileTarget      #> \zs.*'.s:audio_file.'\($\|\ze \[[^\]]*\]$\)# contains=NERDTreeBookmark'
exec 'syn match NERDTreeImageVideoFileTarget #> \zs.*'.s:imagevideo_file.'\($\|\ze \[[^\]]*\]$\)# contains=NERDTreeBookmark'
exec 'syn match NERDTreeArchiveFileTarget    #> \zs.*'.s:archive_file.'\($\|\ze \[[^\]]*\]$\)# contains=NERDTreeBookmark'

exec 'syn match NERDTreeFile                 #^[^"\.'.s:dirArrows.'] *[^'.s:dirArrows.']*# contains=NERDTreeBookmark,NERDTreeLink,NERDTreeExecFile,NERDTreeLibraFile,NERDTreeCodibraFile,NERDTreeAudioFile,NERDTreeImageVideoFile,NERDTreeArchiveFile,NERDTreeExecFileTarget,NERDTreeLibraFileTarget,NERDTreeCodibraFileTarget,NERDTreeAudioFileTarget,NERDTreeImageVideoFileTarget,NERDTreeArchiveFileTarget'

syn match NERDTreeFlags #^ *\zs\[[^\]]*\]# containedin=NERDTreeFile,NERDTreeLink,NERDTreeExecFile,NERDTreeLibraFile,NERDTreeCodibraFile,NERDTreeAudioFile,NERDTreeImageVideoFile,NERDTreeArchiveFile,NERDTreeExecFileTarget,NERDTreeLibraFileTarget,NERDTreeCodibraFileTarget,NERDTreeAudioFileTarget,NERDTreeImageVideoFileTarget,NERDTreeArchiveFileTarget
syn match NERDTreeFlags #\[[^\]]*\]# containedin=NERDTreeDir

"highlighing to conceal the delimiter around the file/dir name
if has("conceal")
    exec 'syn match NERDTreeNodeDelimiters #\%d' . char2nr(g:NERDTreeNodeDelimiter) . '# conceal containedin=ALL'
    setlocal conceallevel=3 concealcursor=nvic
else
    exec 'syn match NERDTreeNodeDelimiters #\%d' . char2nr(g:NERDTreeNodeDelimiter) . '# containedin=ALL'
    hi! link NERDTreeNodeDelimiters Ignore
endif

syn match NERDTreeCWD #^[</].*$#

"highlighting for bookmarks
syn match NERDTreeBookmark # {.*}#hs=s+1

"highlighting for the bookmarks table
syn match NERDTreeBookmarksLeader #^>#
syn match NERDTreeBookmarksHeader #^>-\+Bookmarks-\+$# contains=NERDTreeBookmarksLeader
syn match NERDTreeBookmarkName #^>.\{-} #he=e-1 contains=NERDTreeBookmarksLeader
syn match NERDTreeBookmark #^>.*$# contains=NERDTreeBookmarksLeader,NERDTreeBookmarkName,NERDTreeBookmarksHeader

hi def link NERDTreePart Special
hi def link NERDTreePartFile Type
hi NERDTreeLink           term=bold cterm=bold ctermfg=235 ctermbg=NONE gui=bold guifg=#262626 guibg=NONE

hi NERDTreeExecFile       term=bold cterm=bold ctermfg=196 ctermbg=NONE gui=bold guifg=#FF0000 guibg=NONE
hi NERDTreeLibraFile      term=bold cterm=bold ctermfg=154 ctermbg=NONE gui=bold guifg=#AFFF00 guibg=NONE
hi NERDTreeCodibraFile    term=NONE cterm=NONE ctermfg=22  ctermbg=NONE gui=NONE guifg=#005F00 guibg=NONE
hi NERDTreeAudioFile      term=bold cterm=bold ctermfg=199 ctermbg=NONE gui=bold guifg=#FF00AF guibg=NONE
hi NERDTreeImageVideoFile term=NONE cterm=NONE ctermfg=163 ctermbg=NONE gui=NONE guifg=#DF00AF guibg=NONE
hi NERDTreeArchiveFile    term=NONE cterm=NONE ctermfg=240 ctermbg=NONE gui=NONE guifg=#585858 guibg=NONE

hi def link NERDTreeExecFileTarget       NERDTreeExecFile
hi def link NERDTreeLibraFileTarget      NERDTreeLibraFile
hi def link NERDTreeCodibraFileTarget    NERDTreeCodibraFile
hi def link NERDTreeAudioFileTarget      NERDTreeAudioFile
hi def link NERDTreeImageVideoFileTarget NERDTreeImageVideoFile
hi def link NERDTreeArchiveFileTarget    NERDTreeArchiveFile

hi def link NERDTreeDirSlash Identifier

hi def link NERDTreeBookmarksHeader statement
hi def link NERDTreeBookmarksLeader ignore
hi def link NERDTreeBookmarkName Identifier
hi def link NERDTreeBookmark normal

hi def link NERDTreeHelp String
hi def link NERDTreeHelpKey Identifier
hi def link NERDTreeHelpCommand Identifier
hi def link NERDTreeHelpTitle Macro
hi def link NERDTreeToggleOn Question
hi def link NERDTreeToggleOff WarningMsg

hi def link NERDTreeLinkTarget Normal
hi def link NERDTreeLinkFile Macro
hi def link NERDTreeLinkDir Macro

hi def link NERDTreeDir Directory
hi def link NERDTreeUp Directory
hi def link NERDTreeFile Normal
hi def link NERDTreeCWD Statement
hi def link NERDTreeOpenable Directory
hi def link NERDTreeClosable Directory
hi def link NERDTreeIgnore ignore
hi def link NERDTreeBookmark Statement
hi def link NERDTreeFlags Number

hi def link NERDTreeCurrentNode Search
